# README #

Prerequisites:

- OS: Windows

- Browser: Chrome (Version 89)

- Visual Studio Community 2019 (Choose Universal Windows Platform Development when installing)


Running the tests:

- Open the project in Visual Studio

- Open the Test Explorer window: Test >  Test Explorer

- Build (Ctrl+Shift+B)

- Run the tests
