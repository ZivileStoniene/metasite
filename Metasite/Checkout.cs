﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Metasite
{
    class Checkout
    {
        public static void ProceedToCheckoutStep1(ChromeDriver driver)
        {
            IWebElement ProceedToCheckout = driver.FindElement(By.CssSelector(".standard-checkout"));
            WebDriverExtensions.WaitForElementToBeClickable(ProceedToCheckout, driver);
            ProceedToCheckout.Click();
        }
        public static void CreateAnAccount(ChromeDriver driver)
        {
            Random random = new Random();
            int i = random.Next();
            string email = "zivile.stoniene+" + i.ToString() + "@gmail.com";
            IWebElement EmailFIeld = driver.FindElement(By.CssSelector("#email_create"));
            EmailFIeld.SendKeys(email);
            IWebElement CreateAccount = driver.FindElement(By.CssSelector("#SubmitCreate"));
            CreateAccount.Click();
        }

        public static void FillDetails(ChromeDriver driver)
        {
            //Mandatory fields
            IWebElement Name = driver.FindElementWithTimeout(By.CssSelector("#customer_firstname"));
            WebDriverExtensions.WaitForElementToBeClickable(Name, driver);
            Name.SendKeys("Zivile");
            driver.FindElement(By.CssSelector("#customer_lastname")).SendKeys("Stoniene");
            driver.FindElement(By.CssSelector("#passwd")).SendKeys("Testing123?");
            driver.FindElement(By.CssSelector("#address1")).SendKeys("Street");
            driver.FindElement(By.CssSelector("#city")).SendKeys("City");
            var state = new SelectElement(driver.FindElement(By.Id("id_state")));
            state.SelectByIndex(1);
            driver.FindElement(By.CssSelector("#postcode")).SendKeys("12121");
            driver.FindElement(By.CssSelector("#phone_mobile")).SendKeys("12121212");

            //Not mandatory selections
            driver.FindElementWithTimeout(By.CssSelector("#id_gender2")).Click();
            var day = new SelectElement(driver.FindElement(By.Id("days")));
            day.SelectByIndex(1);
            var month = new SelectElement(driver.FindElement(By.Id("months")));
            month.SelectByIndex(1);
            var year = new SelectElement(driver.FindElement(By.Id("years")));
            year.SelectByIndex(1);
            driver.FindElement(By.CssSelector("#company")).SendKeys("Testing");
            driver.FindElement(By.CssSelector("#other")).SendKeys("Additional comments");
            driver.FindElement(By.CssSelector("#phone")).SendKeys("12121212");

            driver.FindElement(By.CssSelector("#submitAccount")).Click();
        }

        public static void ProceedToCheckoutStep3(ChromeDriver driver)
        {
            IWebElement ProceedToCheckout = driver.FindElement(By.Name("processAddress"));
            WebDriverExtensions.WaitForElementToBeClickable(ProceedToCheckout, driver);
            ProceedToCheckout.Click();
        }

        public static void ProceedToCheckoutStep4(ChromeDriver driver)
        {
            IWebElement ProceedToCheckout = driver.FindElement(By.Name("processCarrier"));
            WebDriverExtensions.WaitForElementToBeClickable(ProceedToCheckout, driver);
            driver.FindElement(By.CssSelector("#cgv")).Click();
            ProceedToCheckout.Click();
        }

        public static void SelectPaymentOption(ChromeDriver driver)
        {
            IWebElement PayByBank = driver.FindElement(By.CssSelector(".bankwire"));
            WebDriverExtensions.WaitForElementToBeClickable(PayByBank, driver);
            PayByBank.Click();
        }

        public static void Confirm(ChromeDriver driver)
        {
            IWebElement Confirm = driver.FindElement(By.CssSelector("#cart_navigation > button"));
            WebDriverExtensions.WaitForElementToBeClickable(Confirm, driver);
            Confirm.Click();
        }

        public static void CheckSuccessMessage(ChromeDriver driver)
        {
            IWebElement Success = driver.FindElement(By.CssSelector("#center_column > div > p > strong"));
            WebDriverExtensions.WaitForElementToBeClickable(Success, driver);
            String SuccessMessage = Success.Text;
            Assert.AreEqual(SuccessMessage, "Your order on My Store is complete.", "Success message not displayed");
        }

    }


}
