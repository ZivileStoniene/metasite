﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;




namespace Metasite
{
    [TestClass]
    public class AutoTest
    {
        [TestMethod]
        public void MakeAnOrder()
        {
            using (var driver = new ChromeDriver())
            {
                //Navigate to shop
                HomePage.GoToApplication(driver);

                //Add product to cart
                HomePage.AddProductToCart(driver);

                //Click proceed to checkout in the popup
                HomePage.ProceedToCheckout(driver);

                //Proceed to checkout in the first checkout step
                Checkout.ProceedToCheckoutStep1(driver);

                //Enter email address and register
                Checkout.CreateAnAccount(driver);

                //Fill account details
                Checkout.FillDetails(driver);

                //Click proceed to checkout in address step
                Checkout.ProceedToCheckoutStep3(driver);

                //Click proceed to checkout in Shipping step
                Checkout.ProceedToCheckoutStep4(driver);

                //Select payment option
                Checkout.SelectPaymentOption(driver);

                //Confirm order
                Checkout.Confirm(driver);

                //Check success message
                Checkout.CheckSuccessMessage(driver);

            }
        }
    }
}
