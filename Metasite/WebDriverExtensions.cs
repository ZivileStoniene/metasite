﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Chrome;

namespace Extensions
{
    public static class WebDriverExtensions
    {
        public static IWebElement FindElementWithTimeout(this IWebDriver driver, By by, int timeoutInSeconds = 25)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
            return wait.Until(drv => drv.FindElement(by));
        }

        public static void MoveToElement(IWebElement element, ChromeDriver driver)
        {

            Actions mouseHover = new Actions(driver);
            mouseHover.MoveToElement(element).Perform();
        }

        public static void WaitForElementToBeClickable(IWebElement element, ChromeDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));

        }

    }
}
