﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using Extensions;

namespace Metasite
{
    class HomePage
    {

        public static void GoToApplication(ChromeDriver driver)
        {
            // Maximize the browser
            driver.Manage().Window.Maximize();
            // Open application
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }

        public static void AddProductToCart(ChromeDriver driver)
        {
            IWebElement Product = driver.FindElement(By.CssSelector("#homefeatured > li.last-item-of-tablet-line.first-item-of-mobile-line"));
            WebDriverExtensions.MoveToElement(Product, driver);
            IWebElement AddToCart = driver.FindElement(By.CssSelector("#homefeatured > li.ajax_block_product.last-item-of-tablet-line.first-item-of-mobile-line > div > div.right-block > div.button-container > .ajax_add_to_cart_button"));
            AddToCart.Click();
        }

        public static void ProceedToCheckout(ChromeDriver driver)
        {
            IWebElement ProceedToCheckout = driver.FindElement(By.XPath("//a[contains(@title,'Proceed to checkout')]"));
            WebDriverExtensions.WaitForElementToBeClickable(ProceedToCheckout, driver);
            ProceedToCheckout.Click();
        }


    }
}
